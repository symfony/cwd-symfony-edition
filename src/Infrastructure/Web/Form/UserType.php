<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Web\Form;

use App\Domain\User\Group;
use App\Domain\User\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return FormBuilderInterface
     */
    public function buildForm(FormBuilderInterface $builder, array $options): FormBuilderInterface
    {
        $builder
            ->add('name', TextType::class, ['label' => 'generic.name'])
            ->add('email', EmailType::class, ['label' => 'user.email'])
            ->add(
                'groups',
                EntityType::class,
                [
                    'class' => Group::class,
                    'choice_label' => 'name',
                    'multiple' => 'multiple',
                    'label' => 'user.groups',
                    'attr' => ['data-toggle' => 'multiple-select', 'class' => 'multiple-select'],
                    'query_builder' => function (\Doctrine\ORM\EntityRepository $er) {
                        $result = $er->createQueryBuilder('o');
                        $result->orderBy('o.name', 'ASC');

                        return $result;
                    },
                ]
            )
            ->add(
                'plainPassword',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'label' => 'user.password',
                    'invalid_message' => 'user.password.mustmatch',
                    'first_options' => ['label' => 'user.password'],
                    'second_options' => ['label' => 'user.password_repeat'],
                ]
            )
        ;

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
                'validation_groups' => function (FormInterface $form) {
                    $data = $form->getData();

                    if (null == $data->getId()) {
                        return ['Default', 'Create'];
                    }

                    return ['Default'];
                },
            ]
        );
    }
}
