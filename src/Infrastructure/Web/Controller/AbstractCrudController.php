<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Web\Controller;

use Cwd\CommonBundle\Controller\AbstractBaseController;
use Cwd\GridBundle\GridBuilderInterface;
use Cwd\GridBundle\GridFactory;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AbstractCrudController extends AbstractBaseController
{
    const INFO = 'info';
    const WARNING = 'warning';
    const ERROR = 'error';
    const DANGER = 'danger';

    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), [
            GridFactory::class,
        ]);
    }

    /**
     * Set default options, set required options - whatever is needed.
     * This will be called during first access to any of the object-related methods.
     *
     * @param OptionsResolver $resolver
     */
    protected function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'entityManager' => EntityManager::class,
            'checkModelClass' => null,
            'grid' => null,
            'gridOptions' => [],
            'redirectRoute' => 'dashboard',
            'createRoute' => null,
            'redirectParameter' => [],
            'successMessage' => 'Data successfully saved',
            'formTemplate' => 'Layout/form.html.twig',
            'title' => null,
            'createLabel' => 'generic.create',
            'createPermission' => '',
        ]);

        $resolver->setRequired([
            'entityFormType',
            'icon',
        ]);
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger()
    {
        return $this->logger;
    }

    /**
     * Session Flashmessenger.
     *
     * @param string      $type
     * @param string|null $message
     */
    protected function flash($type = self::INFO, $message = null)
    {
        $this->get('session')->getFlashBag()->add(
            $type,
            $message
        );
    }

    /**
     * @param array $options
     *
     * @return \Cwd\GridBundle\Grid\GridInterface|null
     *
     * @throws \Cwd\CommonBundle\Exception\InvalidOptionException
     */
    protected function getGrid(array $options = [])
    {
        if (!interface_exists(GridBuilderInterface::class)) {
            throw new \BadMethodCallException('Grid Bundle not present');
        }

        $options = array_merge($this->getOption('gridOptions'), $options);

        return $this->get(GridFactory::class)->create($this->getOption('grid'), $options);
    }

    protected function listHandler(Request $request): array
    {
        $filter = json_decode(urldecode($request->get('filter', '')));
        if (null === $filter) {
            $filter = [];
        }

        $options = [
            'filter' => $filter,
            'limit' => $request->get('limit', 20),
            'page' => $request->get('page', 1),
            'sortField' => $request->get('sortField'),
            'sortDir' => $request->get('sortDir'),
        ];

        return [
            'grid' => $this->getGrid($options),
            'icon' => $this->getOption('icon'),
            'title' => $this->getOption('title'),
            'gridOptions' => $this->getOption('gridOptions'),
            'createRoute' => $this->getOption('createRoute'),
            'createLabel' => $this->getOption('createLabel'),
            'createPermission' => $this->getOption('createPermission'),
        ];
    }
}
