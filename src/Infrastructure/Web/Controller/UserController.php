<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Web\Controller;

use App\Domain\User\Roles;
use App\Domain\User\User;
use App\Domain\User\UserManager;
use App\Infrastructure\Web\Form\UserType;
use App\Infrastructure\Web\Grid\UserGrid;
use Cwd\CommonBundle\Controller\Traits\HandlerTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class UserController.
 *
 * @Route("/user")
 */
class UserController extends AbstractCrudController
{
    use HandlerTrait;

    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), [
            UserManager::class,
            \FOS\UserBundle\Doctrine\UserManager::class,
        ]);
    }

    protected function setOptions()
    {
        $options = [
            'entityManager' => UserManager::class,
            'entityFormType' => UserType::class,
            'grid' => UserGrid::class,
            'redirectRoute' => 'app_infrastructure_web_user_list',
            'icon' => 'fas fa-users',
            'title' => 'menu.users',
            'formTemplate' => 'Layout/form.html.twig',
            'createRoute' => 'app_infrastructure_web_user_create',
            //'createPermission' => Roles::USER_ADD_NEW,
        ];

        return array_merge(parent::setOptions(), $options);
    }

    /**
     * @Route("")
     * Security("is_granted(constant('App\\Domain\\User\\Roles::USER_VIEW_OTHERS'))")
     *
     * @throws \Cwd\CommonBundle\Exception\InvalidOptionException
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        return $this->render(
            'Layout/list.html.twig',
            $this->listHandler($request)
        );
    }

    /**
     * @param Request $request
     * @Route("/create")
     * Security("is_granted(constant('App\\Domain\\User\\Roles::USER_ADD_NEW'))")
     *
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request): Response
    {
        $user = new User();
        $user->setEnabled(true);

        return $this->formHandler($user, $request, true, []);
    }

    /**
     * @param User    $user
     * @param Request $request
     * @Route("/edit/{id}")
     * Security("is_granted(constant('App\\Domain\\User\\Roles::USER_EDIT_OTHERS'))")
     *
     * @return RedirectResponse|Response
     */
    public function editAction(User $user, Request $request): Response
    {
        return $this->formHandler($user, $request, false, []);
    }

    /**
     * @param mixed   $crudObject
     * @param Request $request
     * @param bool    $persist
     * @param array   $formOptions
     *
     * @return RedirectResponse|Response
     */
    protected function formHandler($crudObject, Request $request, $persist = false, $formOptions = array())
    {
        $this->checkModelClass($crudObject);
        $form = $this->createForm($this->getOption('entityFormType'), $crudObject, $formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($persist) {
                    $this->getManager()->persist($crudObject);
                }

                $this->get(\FOS\UserBundle\Doctrine\UserManager::class)->updateUser($crudObject);

                $this->getManager()->flush();

                $this->flashSuccess($this->getOption('successMessage'));

                return $this->redirect(
                    $this->generateUrl($this->getOption('redirectRoute'), $this->getOption('redirectParameter'))
                );
            } catch (\Exception $e) {
                $this->flashError('Error while saving Data: '.$e->getMessage());
                $this->getLogger()->addError($e->getMessage());
            }
        }

        return $this->render($this->getOption('formTemplate'), array(
            'form' => $form->createView(),
            'title' => $this->getOption('title'),
            'icon' => $this->getOption('icon'),
            'redirectRoute' => $this->getOption('redirectRoute'),
            'redirectParameter' => $this->getOption('redirectParameter'),
            'create' => $persist,
        ));
    }

    /**
     * @param User    $user
     * @param Request $request
     * @Route("/switch/{id}")
     * @Security("is_granted('ROLE_ALLOWED_TO_SWITCH')")
     *
     * @return RedirectResponse|Response
     */
    public function switchAction(User $user, Request $request): Response
    {
        return $this->redirect('/?_switch_user='.$user->getUsernameCanonical());
    }

    /**
     * @param User    $user
     * @param Request $request
     * @Route("/delete/{id}")
     * Security("is_granted(constant('App\\Domain\\User\\Roles::USER_EDIT_OTHERS'))")
     *
     * @return RedirectResponse|Response
     */
    public function deleteAction(User $user, Request $request): Response
    {
        return $this->deleteHandler($user, $request);
    }
}
