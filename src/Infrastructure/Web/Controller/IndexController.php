<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Web\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController.
 *
 * @Route("/")
 */
class IndexController extends AbstractController
{
    /**
     * @Route("", name="dashboard")
     *
     * @return Response
     */
    public function dashboardAction(): Response
    {
        return $this->render('Index/dashboard.html.twig');
    }
}
