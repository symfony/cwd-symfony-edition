<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Web\Menu;

use App\Domain\User\Roles;
use KevinPapst\AdminLTEBundle\Event\KnpMenuEvent;
use KevinPapst\AdminLTEBundle\Event\ThemeEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MainMenuSubscriber implements EventSubscriberInterface
{
    private $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ThemeEvents::THEME_SIDEBAR_SETUP_KNP_MENU => ['onSetupMenu', 100],
        ];
    }

    public function onSetupMenu(KnpMenuEvent $event)
    {
        $menu = $event->getMenu();

        $menu->addChild('MainNavigationMenuItem', [
            'label' => 'menu.main_navigation',
            'childOptions' => $event->getChildOptions(),
        ])->setAttribute('class', 'header');

        //if ($this->authorizationChecker->isGranted(Roles::USER_VIEW_OTHERS)) {
        $user = $menu->addChild('user_list',
                [
                    'route' => 'app_infrastructure_web_user_list',
                    'label' => 'menu.users',
                    'childOptions' => $event->getChildOptions(),
                ])
                ->setLabelAttribute('icon', 'fas fa-users');

        $user->addChild('user_create', ['route' => 'app_infrastructure_web_user_create'])->setDisplay(false);
        //}

        /*


        $menu->addChild('blogId', [
            'route' => 'item_symfony_route',
            'label' => 'Blog',
            'childOptions' => $event->getChildOptions()
        ])->setLabelAttribute('icon', 'fas fa-tachometer-alt');

        $menu->getChild('blogId')->addChild('ChildOneItemId', [
            'route' => 'child_1_route',
            'label' => 'ChildOneDisplayName',
            'childOptions' => $event->getChildOptions()
        ])->setLabelAttribute('icon', 'fas fa-rss-square');

        $menu->getChild('blogId')->addChild('ChildTwoItemId', [
            'route' => 'child_2_route',
            'label' => 'ChildTwoDisplayName',
            'childOptions' => $event->getChildOptions()
        ]);
        */
    }
}
