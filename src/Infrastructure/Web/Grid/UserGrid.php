<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Web\Grid;

use App\Domain\User\User;
use App\Infrastructure\Web\Grid\Column\ActionPermissionType;
use Cwd\GridBundle\Column\NumberType;
use Cwd\GridBundle\Column\TextType;
use Cwd\GridBundle\Grid\AbstractGrid;
use Cwd\GridBundle\GridBuilderInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserGrid extends AbstractGrid
{
    /**
     * @param GridBuilderInterface $builder
     * @param array                $options
     */
    public function buildGrid(GridBuilderInterface $builder, array $options)
    {
        $builder->add(new NumberType('id', 'user.id', ['label' => 'generic.id', 'identifier' => true, 'visible' => false]))
            ->add(new TextType('name', 'user.name', ['label' => 'generic.name']))
            ->add(new TextType('email', 'user.email', ['label' => 'user.email']))
            ->add(new TextType('groups_name', 'groups.name', ['label' => 'user.groups', 'template' => 'Grid/groups.html.twig']))
            ->add(new ActionPermissionType(
                'actions',
                'user.id',
                [
                    'label' => null,
                    'minWidth' => '60px',
                    'actions' => [
                        [
                            'route' => 'app_infrastructure_web_user_switch',
                            'class' => 'btn-warning',
                            'icon' => 'fa-unlock',
                            'title' => 'generic.switch',
                            'permission' => 'ROLE_ALLOWED_TO_SWITCH',
                        ],
                        [
                            'route' => 'app_infrastructure_web_user_edit',
                            'class' => 'btn-primary',
                            'icon' => 'fa-edit',
                            'title' => 'generic.edit',
                            'permission' => 'ROLE_SUPER_ADMIN',
                        ],
                        [
                            'route' => 'app_infrastructure_web_user_delete',
                            'class' => 'btn-danger deleterow',
                            'icon' => 'fa-trash-alt',
                            'title' => 'generic.delete',
                            'permission' => 'ROLE_SUPER_ADMIN',
                        ],
                    ],
                ]
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'template' => 'Layout/grid.html.twig',
            'pagerfantaOptions' => [
                'prev_message' => $this->translator->trans('generic.prev'),
                'next_message' => $this->translator->trans('generic.next'),
            ],
        ]);
    }

    /**
     * @param ObjectManager $objectManager
     * @param array         $params
     *
     * @return QueryBuilder
     */
    public function getQueryBuilder(ObjectManager $objectManager, array $params = []): QueryBuilder
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $objectManager
            ->getRepository(User::class)
            ->createQueryBuilder('user')
            ->leftJoin('user.groups', 'groups')
            ->addOrderBy('user.name', 'ASC');

        return $queryBuilder;
    }
}
