<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Domain\User;

use Cwd\CommonBundle\Manager\AbstractManager;
use Doctrine\ORM\EntityNotFoundException;

class UserManager extends AbstractManager
{
    /**
     * Set raw option values right before validation. This can be used to chain
     * options in inheritance setups.
     *
     * @return array<string,string>
     */
    protected function setServiceOptions()
    {
        return [
            'modelName' => User::class,
            'notFoundExceptionClass' => EntityNotFoundException::class,
        ];
    }
}
