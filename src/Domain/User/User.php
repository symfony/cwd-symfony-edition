<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Domain\User;

use FOS\UserBundle\Model\User as FOSUser;
use Gedmo\Blameable\Traits\Blameable;
use Gedmo\Timestampable\Traits\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;

class User extends FOSUser
{
    use Timestampable;
    use Blameable;

    /**
     * @var string|null
     * @Assert\NotBlank(groups={"Default", "Create"})
     */
    protected $name;

    /**
     * @var string|null
     * @Assert\NotBlank(groups={"Create"})
     * @Assert\Length(min=8, groups={"Create"})
     */
    protected $plainPassword;

    /**
     * @var string|null
     * @Assert\Email(groups={"Default", "Create"})
     * @Assert\NotBlank(groups={"Default", "Create"})
     */
    protected $email;

    /**
     * @var string[]
     */
    protected $roles = [];

    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDisplayName(): string
    {
        if (null === $this->name) {
            return $this->getEmail();
        }

        return $this->getName();
    }

    /**
     * @param string|null $name
     *
     * @return User
     */
    public function setName(?string $name): User
    {
        $this->name = $name;

        return $this;
    }
}
