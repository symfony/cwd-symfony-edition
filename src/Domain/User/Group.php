<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Domain\User;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\Group as FOSGroup;
use Symfony\Component\Validator\Constraints as Assert;

class Group extends FOSGroup
{
    /**
     * @var string|null
     * @Assert\NotBlank
     * @Assert\Length(max=180)
     */
    protected $name;

    /**
     * @var array
     * @Assert\Count(min=1)
     */
    protected $roles;

    /** @var string|null */
    protected $id;

    /** @var User[] */
    protected $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->roles = [];
    }
}
